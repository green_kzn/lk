@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form class="form" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="logo-xs">
                    <a href="{{\App\Http\Controllers\InfoHelper::getSettings('logo_link')}}">
                        <img src="/logo">
                    </a>
                </div>
                <span class="text">
                    Вход в личный кабинет
                </span>

                <div class="group1">
                    <span class="text">
                        Номер телефона
                    </span>

                    <input type="text" class="form-control reg-form-phone @error('name') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="name" autofocus>

                    @error('mane')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="group2">
                    <span class="text">
                        Пароль
                    </span>
                    <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="remember">
                    <div class="remember-checkbox-xs">
                        <input type="checkbox" class="checkbox" class="form-check-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}/>
                        <label class="text-reg" >Запомнить меня</label>
                    </div>
{{--                    <label for="remember">Запомнить меня</label>--}}
                </div>

                <div class="forgot">
                    <a href="{{ route('password.request') }}">Забыли пароль?</a>
                </div>

                <div style="clear: both;"></div>
                @if($errors->any())
                    <div class="error" style="display: block;">
                        <span>{{$errors->first()}}</span>
                    </div>
                @endif

                <button type="submit">Войти</button>

                <hr>

                <div class="reg" style="text-align: center">
                    <span class="text-reg">Нажимая "Войти", я принимаю согласие на </span><a style="color:#53789C" href="/agreement" class="link-reg">обработку персональных данных</a>
                </div>


                <div class="reg">
                    <span class="text-reg">Если у Вас нет аккаунта?</span><a href="/register-sms" class="link-reg"> Зарегистрируйтесь</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
