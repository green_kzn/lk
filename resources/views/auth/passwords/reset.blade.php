@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form class="form" method="POST" action="">
                @csrf
                <div class="logo-xs">
                    <a href="/">
                        <img src="/img/logo.svg">
                    </a>
                </div>
                <span class="text">
                    Восстановление пароля
                </span>

                <div class="group1">
                    <span class="text">
                        Новый пароль
                    </span>
                    <input type="password" id="password" name="password">
                </div>

                <div class="group2">
                    <span class="text">
                        Подтверждение пароля
                    </span>
                    <input type="password" id="password_confirmation" name="password_confirmation">
                </div>

                <div style="clear: both;"></div>
                @if($errors->any())
                    <div class="error" style="display:block;">
                        <span>{{$errors->first()}}</span>
                    </div>
                @endif

                <div class="error">
                    <span></span>
                </div>

                <button type="submit">Восстановить</button>
            </form>
        </div>
    </div>
</div>
@endsection
