@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form class="form" method="POST" action="{{ route('register') }}">
                @csrf
                <div class="logo-xs">
                    <a href="/">
                        <img src="/img/logo.svg">
                    </a>
                </div>
                <span class="text">
                    Регистрация
                </span>

{{--                <div class="group1">--}}
{{--                    <span class="text">--}}
{{--                        Логин--}}
{{--                    </span>--}}
{{--                    <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>--}}
{{--                </div>--}}

                <div class="group2">
                    <span class="text">
                        Пароль
                    </span>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                </div>

                <div class="group2">
                    <span class="text">
                        Подтверждение пароля
                    </span>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>

                <div style="clear: both;"></div>
                @if($errors->any())
                    <div class="error" style="display: block;">
                        <span>{{$errors->first()}}</span>
                    </div>
                @endif

                <button type="submit">Зарегистрироваться</button>
            </form>
        </div>
    </div>
</div>
@endsection
