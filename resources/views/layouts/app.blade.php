<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<link rel="stylesheet" href="{{ asset('css/style_v3_21_0.css') }}?v={{env('VERSION_CSS2')}}">
	<script src="{{ asset('js/jquery-3.4.1.min.js') }}" type="text/javascript"></script>
{{--	<script src="{{ asset('js/jquery.maskedinput.min.js') }}" type="text/javascript"></script>--}}
	<script src="{{ asset('js/jquery.mask.js') }}" type="text/javascript"></script>
{{--    <script src="{{ asset('js/colors.js') }}"></script>--}}
    <style>
        body {
            --main: {{$main}};
            --shadow: {{$shadow}};
        }
    </style>
</head>
<body>
	<div class="left">
		<div class="logo">
			<a href="{{\App\Http\Controllers\InfoHelper::getSettings('logo_link')}}">
				<img src="/logo" width="20%">
			</a>
		</div>
		    @yield('content')
{{--		</div>--}}
	</div>
	<div class="right">
		<img src="{{$rightImg}}" alt="">
	</div>
    <script src="{{ asset('js/phone-mask.js') }}"></script>
	<script>
		$(document).ready(function() {
			// $("#phone").mask('+7 (999) 999 99 99');
			// $("#phone2").mask('+7 (999) 999 99 99');

            const SEC = 59;
            var codeBtn = $('.code-btn');
            var msg = $('#msg');
            var sendAgain =  $('#send_again');

			function showError(error, hide = true) {
			    $('.error').not('#send_again').hide();
                msg.find('span').text(error);
                msg.css('display', 'block');
				if (hide) {
                    setTimeout(function () {
                        msg.find('span').text('');
                        msg.hide()
                    }, 7000);
                }
				return;
			}

			function startTimeout(){
                let seconds = SEC;
                var timeout = setInterval(function () {
                    showError(`Отправить код ещё раз через ${seconds} сек`, false);
                    sendAgain.hide()
                    if (seconds <= 0){
                        clearInterval(timeout)
                        codeBtn.addClass('active')
                        $(".error").hide()
                        $('#send_again').show();
                    }
                    seconds--;
                }, 1000)
            }

            $('#phone').on('input', function () {
                if (checkPhone(this.value)){
                    codeBtn.addClass('active')
                } else {
                    codeBtn.removeClass('active')
                }
            })

            codeBtn.on('click', sendCode)
            sendAgain.on('click', sendCode);

            function sendCode() {
                if (codeBtn.hasClass('active')){
                    codeBtn.removeClass('active');
                    loading(true)
                    var phone = $("#phone").val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: 'POST',
                        url: codeBtn.data('action'),
                        data: {'phone': phone},
                        success: function(resp) {
                            loading(false)
                            if (resp.status == false) {
                                codeBtn.addClass('active')
                                showError(resp.sms_status);
                            } else {
                                startTimeout();
                                showCodeFields();
                            }

                        }
                    });
                }
            }
            function checkPhone(phone) {
                return /\+7\(\d{3}\)\d{3}-\d{2}-\d{2}/.test(phone)
            }

            function loading(show) {
			    if (show) {
                    $('.code-btn .loading').show()
                } else {
                    $('.code-btn .loading').hide()
                }
            }

            function showCodeFields() {
                codeBtn.hide();
                $('.group2').show();
                $('#submit').show();
            }
		})
	</script>
</body>
</html>
