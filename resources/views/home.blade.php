<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=970" name="viewport" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=yes"/>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}?v={{env('VERSION_CSS')}}">
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}" type="text/javascript"></script>

{{--    <script src="{{ asset('js/colors.js') }}"></script>--}}
    <script src="{{ asset('js/functions.js') }}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ asset('css/sprite.css') }}">
    <style>
        body {
            --main: {{$main}};
            --shadow: {{$shadow}};
        }
    </style>
</head>
<body class="home">
    <div class="wrapper">
        <div class="content">
            <div id="app">
                <App :bus="bus" :user="user" :users="users" :is-info-loading="loading" :hospital="hospital"></App>
            </div>
        </div>
    </div>

    <script src="{{ asset('/js/app_v'.env('APP_VERSION').'.js')}}?v={{env('VERSION_JS')}}"></script>
{{--    <script src="{{ asset('js/scripts.js') }}?v={{env('VERSION_SCRIPTS')}}"></script>--}}
    <script src="{{ asset('js/bower_components/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('js/bower_components/svg4everybody/dist/svg4everybody.js') }}"></script>
</body>
</html>
