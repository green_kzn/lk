@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form class="form" method="POST" action="/register-sms">
                @csrf
                <div class="logo-xs">
                    <a href="/">
                        <img src="/img/logo.svg">
                    </a>
                </div>
                <span class="text">
                    Регистрация
                </span>

                <div class="group1">
                    <span class="text">
                        Номер телефона
                    </span>
                    <input type="text" id="phone" class="reg-form-phone" name="phone" placeholder="+7 (999) 999-99-99" value="{{old('phone')}}">
                </div>

                <div class="group2" @if($errors->any()) style="display:none" @endif>
                    <div class="code-btn @if($errors->any()) active @endif"  data-action="/register/sendsms">
                        Получить код
                        <img class="loading" style="display: none" width="14" src="/img/load.gif" alt="loading">
                    </div>
                </div>

                <div class="group2" @if(!$errors->any()) style="display:none" @endif>
                    <span class="text">
                        Код подтверждения
                    </span>
                    <input type="text" id="code" name="code">

                </div>

                <div style="clear: both;"></div>
                @if($errors->any())
                    <div class="error" style="display:block;">
                        <span>{{$errors->first()}}</span>
                    </div>
                @endif

                <div class="error" id="msg">
                    <span></span>
                </div>
                <div class="error" id="send_again" @if($errors->any()) style="display:block" @endif>
                    <span><a href="#" style="color: black">Отправить код заново</a></span>
                </div>

                <button id="submit" style="display: none" type="submit">Зарегистрироваться</button>
            </form>
        </div>
    </div>
</div>
@endsection
