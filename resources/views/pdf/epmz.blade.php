<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Выписка</title>
    <style>
        body { font-family: DejaVu Sans, sans-serif; }
        .table th{
            /*border: 1.6px solid black;*/
            padding: 4px;
            text-align: center;
        }
        .table td {
            /*border: 1px solid black;*/
            padding: 4px;
        }
        .table {
            margin-top: 40px;
            /*border-collapse: collapse;*/
            /*border: 1.6px solid black;*/
            width: 100%
        }
        .s1 {
            font-size: 13px;
            /*color: #000000; font-style: normal;*/
            background-color: transparent;
            text-align: right;
            vertical-align: Top;
            padding-top: 1px;
            padding-left: 2px;
        }
        .line {
            margin: 10px 0;
            height: 1px;
            background: #1b1e21;
        }
    </style>
</head>
<body>
@if(!isset($noHeader))
<table cellspacing="0" cellpadding="0" style="width: 100%;margin-bottom: 26px">
    <tr style="height:84px">
        <td colspan="1" class="s0" style="font-size:1px"><img src="{{ $clinic['logo'] }}" width="228" height="88" alt="">
        </td>
        <td colspan="3" class="s1">
            {!! nl2br($clinic['info']) !!}
        </td>
    </tr>
</table>

<div class="line"></div>
@endif
<div style="text-align:center"><h2>{{ $epmz->name }}</h2></div>

<table cellspacing="3" cellpadding="4" style="width: 100%">
    <tr>
        <td><b>Амб. карта</b></td>
        <td>{{$user->NUMBER}}</td>
        <td><b>ФИО</b></td>
        <td>{{$user->FULLNAME}}</td>
    </tr>
    <tr>
        <td><b>Дата рождения</b></td>
        <td>{{$user->BIRTHDATE}}</td>
        <td><b>Пол</b></td>
        <td>{{$sex}}</td>
    </tr>
    <tr>
        <td><b>Адрес</b></td>
        <td colspan="3">{{$user->ADDRESS_LOCALITY.' ул.'.$user->ADDRESS.' д.'.$user->ADDRESS_HOUSE.''.$user->ADDRESS_BUILD.' кв.'.$user->ADDRESS_FLAT}}</td>
    </tr>
    <tr>
        <td><b>Дата посещения</b></td>
        <td colspan="2">{{ $epmz->date }}</td>
    </tr>
</table>

<table class="table">
    <tbody>
    @foreach($elements as $k => $v)
        <tr>
            <td style="width: 30%"><b>{{$k}}</b></td>
            <td style="width: 70%">{{$v}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<table class="table">
    <tbody>
        <tr>
            <td style="width: 30%"><b>Врач</b></td>
            <td style="width: 70%">{{ $epmz->doctor }}</td>
        </tr>
    </tbody>
</table>
</body>
</html>
