<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Результаты лабораторного исследования</title>
    <style>
        body { font-family: DejaVu Sans, sans-serif; }
        .table th{
            border: 1.6px solid black;
            padding: 4px;
            text-align: center;
        }
        .table td {
            border: 1px solid black;
            padding: 4px;
        }
        .table {
            border-collapse: collapse;
            border: 1.6px solid black;
            width: 100%
        }
        .s1 {
            font-size: 13px;
            /*color: #000000; font-style: normal;*/
            background-color: transparent;
            text-align: right;
            vertical-align: Top;
            padding-top: 1px;
            padding-left: 2px;
        }
        .line {
            margin: 10px 0;
            height: 1px;
            background: #1b1e21;
        }
    </style>
</head>
<body>
@if(!isset($noHeader))
<table cellspacing="0" cellpadding="0" style="width: 100%;margin-bottom: 26px">
    <tr style="height:84px">
        <td colspan="1" class="s0" style="font-size:1px"><img src="{{ $clinic['logo'] }}" width="228" height="88" alt="">
        </td>
        <td colspan="3" class="s1">
            {!! nl2br($clinic['info']) !!}
        </td>
    </tr>
</table>

<div class="line"></div>
@endif
<div style="text-align:center"><h2>Результаты лабораторного исследования</h2></div>

<table cellspacing="3" cellpadding="4" style="width: 100%">
    <tr>
        <td><b>Амб. карта</b></td>
        <td>{{$user->NUMBER}}</td>
        <td><b>ФИО</b></td>
        <td>{{$user->FULLNAME}}</td>
    </tr>
    <tr>
        <td><b>Дата рождения</b></td>
        <td>{{$user->BIRTHDATE}}</td>
        <td><b>Пол</b></td>
        <td>{{$sex}}</td>
    </tr>
    @if(!empty($user->ADDRESS_LOCALITY))
    <tr>
        <td><b>Адрес</b></td>
        <td colspan="3">{{$user->ADDRESS_LOCALITY.' ул.'.$user->ADDRESS.' д.'.$user->ADDRESS_HOUSE.''.$user->ADDRESS_BUILD.' кв.'.$user->ADDRESS_FLAT}}</td>
    </tr>
    @endif
    <tr>
        <td><b>Дата посещения</b></td>
        <td>{{$analyze[0]->DoneDate}}</td>
        <td><b>Дата готовности</b></td>
        <td>{{$analyze[0]->visit_date}}</td>
    </tr>
</table>

<table class="table" style="">
    <tbody>
    <tr>
        <th colspan="4">{{$analyze[0]->name}}</th>
    </tr>
    <tr>
        <td style="width: 5%">№ п/п</td>
        <td style="width: 45%">Показатель</td>
        <td style="width: 25%">Результат</td>
        <td style="width: 25%">Норма</td>
    </tr>

    @foreach($analyze as $k => $a)
        <tr>
            <td style="width: 5%">{{$k+1}}</td>
            <td style="width: 45%">{{$a->FieldName}}</td>
            <td style="width: 25%">{{$a->StrValue}}</td>
            <td style="width: 25%">{{$a->NormText}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
