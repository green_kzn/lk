/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import App from './components/App.vue';
import Login from './components/Login.vue';
import Nav2 from './components/Nav.vue';
import Header2 from './components/Header.vue';
import Home from './components/Home.vue';
import Zapis from './components/Zapis.vue';
import Analyze from './components/Analyze.vue';
import Extracts from './components/Extracts.vue';
import Program from './components/Program.vue';
import Financial from './components/Financial.vue';
import Request from "./components/Request";
import Preloader from "./components/popups/Preloader";
import ProgramService from "./components/ProgramService";

import VueSelect from 'vue-select';
import 'vue-select/dist/vue-select.css'



require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);
Vue.component('v-select', VueSelect)

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('nav2', require('./components/Nav.vue').default);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/home',
            name: 'Home',
            component: Home,
        },
        {
            path: '/zapis',
            name: 'Zapis',
            component: Zapis,
        },
        {
            path: '/analyze',
            name: 'analyze',
            component: Analyze,
        },
        {
            path: '/extracts',
            name: 'extracts',
            component: Extracts,
        },
        {
            path: '/program',
            name: 'program',
            component: Program,
        },
        {
            path: '/program/:id',
            name: 'programServices',
            component: ProgramService,
        },
        {
            path: '/financial',
            name: 'financial',
            component: Financial,
        },
    ]
})

Vue.directive('phone', {
    bind(el) {
      el.oninput = function(e) {
        if (!e.isTrusted) {
          return;
        }

        let x = this.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        this.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
        el.dispatchEvent(new Event('input'));
      }
    }
});

const app = new Vue({
    el: '#app',
    components: {
        App,
        Login,
        Nav2,
        Header2,
        Request,
        Preloader
    },
    router,
    data() {
        return {
            loading: false,
            irLink: '',
            hospital: {},
            user: {},
            users: [],
            bus: new Vue(),
            noUserFound: false,
        }
    },
    create: {
        // listenForBroadcast(survey_id) {
        //     console.log(survey_id);
        //     Echo.join('private-message123')
        //       .here((users) => {
        //         console.log('Ok2');
        //         this.users_viewing = users;
        //         this.$forceUpdate();
        //       })
        //       .joining((user) => {
        //         console.log('Ok3');
        //         if (this.checkIfUserAlreadyViewingSurvey(user)) {
        //           this.users_viewing.push(user);
        //           this.$forceUpdate();
        //         }
        //       })
        //       .leaving((user) => {
        //         this.removeViewingUser(user);
        //         this.$forceUpdate();
        //       });
        // }
    },
    created() {
        this.getInfo();
        this.bus.$on('user-changed', this.changeUser)
    },
    methods: {
        getInfo(){
            this.loading = true
            axios.get('/info').then(response => {
                // console.log(response)
                this.hospital = response.data.hospital
                this.irLink = response.data.irLink
                this.user = response.data.user
                this.users = response.data.users

                if (this.user){
                    this.bus.$emit('load-next')
                }

                this.setRoistat(response.data['roistat_key']);
            })
            .finally(() => (
                this.loading = false

            ))
        },
        changeUser(id){
            this.loading = true
            axios.get('/changeUser/'+id).then(response => {
                this.user = response.data.user
            })
                .finally(() => (
                    this.loading = false
                ))
        },
        setRoistat(roistatKey){
            console.log('Roistat: ' + (roistatKey ? 'Да. '+ roistatKey : 'Нет'))
            if (roistatKey){
                (function(w, d, s, h, id) {
                    w.roistatProjectId = id; w.roistatHost = h;
                    var p = d.location.protocol == "https:" ? "https://" : "http://";
                    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
                    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
                })(window, document, 'script', 'cloud.roistat.com', roistatKey);
            }
        }
    }
});
