<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
App::setLocale('ru');
if (isset($_REQUEST['clear'])){
    Artisan::call('view:clear');
}

Route::group(['middleware' => 'visitors'], function() {
    Route::get('/', function () {
        return redirect()->route('login');
    });

    Route::get('/register-sms', function () {
        return view('register-sms', \App\Http\Controllers\CustomLoginController::getVars());
    })->name('register-sms');


    Route::post('/register/sendsms', 'RegController@getCode');
    Route::post('/register-sms', 'RegController@register');
    Route::post('/password-reset', 'RegController@resetCode');
    Route::post('/reset/code', 'RegController@resetPassword');
    Route::get('/reset', 'RegController@reset')->name('reset');
    Route::post('/reset', 'RegController@postReset');

    Auth::routes();
    Route::post('/login', 'CustomLoginController@login');
    Route::get('/login', 'CustomLoginController@index')->name('login');
    Route::get('/agreement', 'AgreementController@index')->name('agreement');
});

Route::get('/logo', 'Logo@index');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/zapis', 'HomeController@index')->name('zapis');
Route::get('/analyze', 'HomeController@index')->name('analyze');
Route::get('/extracts', 'HomeController@index')->name('extracts');
Route::get('/program', 'HomeController@index')->name('program');
Route::get('/program/{id}', 'HomeController@index');
Route::get('/financial', 'HomeController@index')->name('financial');

Route::group(['middleware' => 'cabinet'], function() {
    Route::get('/user/getUser', 'HomeController@getUser');

    Route::get('/zapis/getTalons', 'ZapisController@getTalons')->name('zapis');
    Route::post('/talons/filter', 'ZapisController@filter')->name('filter');

    Route::get('/analyze/getAnalyze', 'AnalyzeController@getAnalyze');
    Route::get('/analyze/pdf/{code}', 'AnalyzeController@pdf');
    Route::get('/analyze/email/{code}', 'AnalyzeController@send');

    Route::get('/extracts/getEpmz', 'ExtractsController@getEpmz');
    Route::get('/extracts/pdf/{id}', 'ExtractsController@pdf');
    Route::get('/extracts/email/{id}', 'ExtractsController@send');

    Route::get('api/program', 'ProgramController@getProgram');
    Route::get('api/program/{id}', 'ProgramController@getProgramServices');
//    Route::get('/program/getProgramServ/{id}', 'ProgramController@getProgramServ');

    Route::get('/financial/getFin', 'FinancialController@getFin');
//    Route::get('/tcpdf/{id}', 'HomeController@tcpdf')->name('tcpdf');
//    Route::get('/email/{id}', 'HomeController@email')->name('email');
    Route::get('/sendRequest', 'RequestController@send')->name('sendRequest');
    Route::get('/info', 'MainController@info');
    Route::get('/changeUser/{id}', 'MainController@changeUser');
});

Route::get('/get-conn', 'ConnController@index');
