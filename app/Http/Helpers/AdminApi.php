<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;

class AdminApi
{

    private static $project = 'lk';

    private static $url = 'http://admin.archimed-soft.ru/';

    public static function getAll() {
        $token = base64_encode(ReversCrypt::mc_encrypt(self::$project));
        $all = file_get_contents(self::$url.'api/get-all?token='.$token);
        return json_decode(ReversCrypt::mc_decrypt($all));
    }

    public static function getBy($value, $field) {
        $token = base64_encode(ReversCrypt::mc_encrypt(self::$project));
        $options = file_get_contents(self::$url."api/get-by?filters[0][field]=$field&filters[0][value]=$value&token=".$token);
        return json_decode(ReversCrypt::mc_decrypt($options));
    }

    public static function get($token) {
        $options = file_get_contents(self::$url.'api/get-options?project='.self::$project.'&token='.$token);
        return json_decode(ReversCrypt::mc_decrypt($options));
    }

    public static function getOtherProject($token, $project) {
        $options = file_get_contents(self::$url.'api/get-options?project='.$project.'&token='.$token);
        return json_decode(ReversCrypt::mc_decrypt($options));
    }
}
