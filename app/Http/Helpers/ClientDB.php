<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;

class ClientDB  {
    protected $config;
    protected $db;
    private $defaultLimit = 100;
    private $maxLimit = 10000;
    private $errorSqlResponse = ['error'=>400,'message'=>'Query error'];
    private $errorNoConnection = ['error'=>400,'message'=>'No DB connection'];
    private $isPagination = false; // Для корректной работы order by
    private $default = []; // Настройки запроса
    private $queryBuilder;
    private $fromRequest = false;

    private $errorHandler = null;
    private $sep = ' ';
    /**
     * ClientDB constructor.
     * @param $connection
     * @param callable $logHandler
     */
    public function __construct() {
        $this->db = app('db')->connection('sqlsrv');
        $this->resetQueryBuilder();
        $this->sep = isset($_REQUEST['dev']) ? "\n" : ' ';
    }

    public function select($sql) {
        return $this->db->select($sql);
    }

//    public function update($sql) {
//        return $this->db->update($sql);
//    }

    public function procedure($name, $args = [], $fetch = true) {
        $name .= ' '.implode(', ', array_map(function ($item) { return ':'.$item; }, array_keys($args)));
        $sql = "EXEC dbo.$name";
        return $this->execute($sql, $args, $fetch);
    }

    /**
     * Здесь будет реализована пагинация. В $request будет передаваться (или нет) параметр page и limit.
     * от этого будут выдаваться записи. Если не передана страница то берется значение по умолчанию
     * @param String $sql
     * @param Request|null $request
     * @param array $args
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function selectWithPagination($sql, Request $request = null, $args = []) {
        $page = $_REQUEST['page'] ?? 1;
        $limit = $_REQUEST['limit'] ?? $this->defaultLimit;
        if ($limit > $this->maxLimit) $limit = $this->maxLimit;

        $from = intval(($page - 1) * $limit + 1);
        $to = intval($page * $limit);

        $matches = $this->getMatches($sql);
        $sql = $this->putRowNumberAfterSelect($sql, $matches);

        $sql = "SELECT * FROM (" . $sql . ") T
                WHERE T.Num BETWEEN $from AND $to";
        $sql = $this->getVariableStr().$sql;

        $array = $this->execute($sql, $args, true);
        $totalPages = $this->getTotalPages($array);

        return [
            'data' => $array,
            'total' => $totalPages,
            'page' => intval($page),
            'limit' => intval($limit)
        ];
    }

    private function putRowNumberAfterSelect($sql, $matches) {
        if ($matches) {
            $order = $this->queryBuilder['orderBy'];
            $orderBy = $this->isPagination && isset($order) ? "$order[0] $order[1]": $matches['firstColumn'];
            $sql = mb_substr($sql, 0, $matches['posStart']) .
                " ROW_NUMBER() OVER(order by {$orderBy}) AS Num
                  , SUM(1) OVER() AS total_count, " .
                mb_substr($sql, $matches['posStart']);
        }

        return $sql;
    }

    private function getTotalPages(&$array) {
        /* Получаем общее количество записей
         * В $array собержитсья столбец total_count -> общее количество записей
         * Выбираем из 1-ой записи total_count. И дальше удалеем во всех записях total_count
         * Возврает общее количество записей -> type(int)*/
        if (!empty($array) and $array[0]['total_count']){
            $total = $array[0]['total_count'];
            foreach ($array as &$item) {
                unset($item['total_count']);
            }
            return intval($total);
        }
        return 0;
    }

    private function getMatches($sql){
        preg_match('/select|SELECT/', $sql, $matchStart, PREG_OFFSET_CAPTURE);
        preg_match('/from|FROM/', $sql, $matchFrom, PREG_OFFSET_CAPTURE);
        preg_match('/ as | AS |,| from | FROM /', $sql, $matchFirst, PREG_OFFSET_CAPTURE);
        if ($matchStart[0] && $matchFrom[0] && $matchFirst[0]) {
            $posStart = strlen($matchStart[0][0]) + $matchStart[0][1];
            $firstColumn = trim(mb_substr($sql, $posStart, $matchFirst[0][1] - $posStart));
            return [
                'posStart' => $posStart,
                'posFrom' => $matchFrom[0][1],
                'firstColumn' => $firstColumn,
            ];
        }
        return false;
    }

    public function variable($vars = []){
        $this->queryBuilder['vars'][] = $vars;
        return $this;
    }

    public function sysVariable($varName, $sysName, $int = false){
        $type = $int ? 'Int' : 'Varchar(1024)';
        $this->queryBuilder['vars'][] = [$type, $varName, "dbo.get_sys_value_int('$sysName')"];
        return $this;
    }

    /**
     * Настравает queryBuilder, устанавливая поля которые надо вывести
     * @param array $fields
     * @return $this
     */
    public function fields($fields = []){
        $this->queryBuilder['fields'] = $fields;
        return $this;
    }

//    /**
//     * Инициализация настроек
//     * @param $def
//     * @return $this
//     */
//    public function default($def){
//        $this->default = $def;
//        $this->leftJoinsFromDef();
//        return $this;
//    }
//
//    /**
//     * Работает только если установлен $this->default(для безопасности)
//     * Добавляет поля из запроса, сравнивая с настройками $this->default
//     * важно! вызывать только после инициализации: default()
//     * Нельзя комбинировать с fieldsFromDef
//     * @return $this
//     */
//    public function fieldsFromRequest(){
//        $this->queryBuilder['fields'] = $this->getFields();
//        $this->fromRequest = true;
//        return $this;
//    }
//
//    /**
//     * Добавляет left join из настроек $this->default
//     * важно! вызывать только после инициализации: default()
//     * @return $this
//     */
//    private function leftJoinsFromDef(){
//        $left = $this->default['left'] ?? [];
//        foreach ($left as $item) {
//            $this->leftJoin($item['name'], $item['short'], $item['field1'], $item['field2'],
//                $item['co'] ?? '=', $item['enabled'] ?? false,
//                $item['type'] ?? 'left join', $item['orderBy'] ?? '');
//        }
//        return $this;
//    }
//
//    /**
//     * Нельзя комбинировать с fieldsFromRequest
//     * @return $this
//     */
//    public function fieldsFromDef(){
//        $this->queryBuilder['fields'] = $this->getFields(true);
//        return $this;
//    }
    public function from($tableName, $short = null, $enabled = true) {
        if ($this->default && isset($this->default['dbName'])) $tableName = $this->default['dbName'];
        $this->queryBuilder['from'] = $tableName;
        if (!isset($short)){
            $short = mb_substr($tableName, 0, 1);
        }
        $this->queryBuilder['short'] = $short;
        $this->queryBuilder['enabled'] = $enabled;
        return $this;
    }
    public function where($field, $value, $c = '=', $fieldObj = null, $if = true){
        if ($if) $this->queryBuilder['where'][] = [$field, $c, $value, $fieldObj];
        return $this;
    }
    public function whereRaw($raw, $if = true){
        if ($if) $this->queryBuilder['whereRaw'][] = $raw;
        return $this;
    }
    public function leftJoin($table, $short, $field1, $field2, $co = '=', $enabled = false, $type = 'left join', $orderBy = ''){
        $this->queryBuilder['leftJoin'][] = [$table, $short, $field1, $co, $field2, $enabled, $type, $orderBy];
        return $this;
    }

    /**
     * @param $field
     * @param string $dir 'desc' or 'acs'
     * @return $this
     */
    public function orderBy($field, $dir = ''){
        $this->queryBuilder['orderBy'] = [$field, $dir];
        return $this;
    }

    public function groupBy(...$field){
        $this->queryBuilder['groupBy'] = array_merge($this->queryBuilder['groupBy'], $field);
        return $this;
    }

//    /**
//     * Работает только если установлен $this->default(для безопасности)
//     * Берет из запроса sortBy и sortType
//     * и если $this->isPagination == true подставляет в оконную функцию, см. putRowNumberAfterSelect()
//     * иначе в конец запроса
//     * @return $this
//     */
//    public function orderByFromRequest(){
//        $sortBy = $_REQUEST['sortBy'] ?? null;
//        $sortType = $_REQUEST['sortType'] ?? 1; // по умолчанию 1
//        if (isset($sortBy)){
//            $field = $this->getFieldByName($sortBy);
//            if ($field) $this->orderBy($field['dbName'] ?? $field['name'], $sortType == 1 ? '' : 'desc');
//        }
//        return $this;
//    }

    /**
     * Выполняет запрос и возвращает первую запись
     * @return object|array
     */
    public function first(){
        $this->queryBuilder['first'] = true;
        $res = $this->get();
        if (!empty($res)){
            return $res[0];
        } else {
            return [];
        }
    }

    /**
     * Выполняет запрос и возвращает данные
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function get(){
        $this->isPagination = false;
        $res = $this->execute($this->getSql(), $this->getWhereArgs(), true);

        $this->resetQueryBuilder();
        return $res;
    }

//    /**
//     * Добавляет в условие where фильтры из $_REQUEST
//     * важно! вызывать только после инициализации: default()
//     * @return $this
//     */
//    public function filtersFromRequest(){
//        $error = response(['error'=>'400', 'message'=>'Invalid array filters']);
//        $filters = $_REQUEST['filters'] ?? [];
//        if (!is_array($filters)) $error->send();
//        foreach ($filters as $filter) {
//            if (!isset($filter['field']) || !isset($filter['value'])) $error->send();
//            if ($field = $this->getFieldByName($filter['field'])){
//                $dbName = $field['dbName'] ?? $field['name'];
//                $this->where($dbName, $filter['value'], $filter['co'] ?? '=', $field);
//            }
//        }
//        return $this;
//    }

    /**
     * Выполняет запрос и возвращает данные с пагинацией
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function pagination(){
        $this->isPagination = true;
        $res = $this->selectWithPagination($this->getSql(), null, $this->getWhereArgs());
        $this->resetQueryBuilder();
        return $res;
    }

    /**
     * Собтрает sql запрос из queryBuilder
     * @param bool $withParams
     * @return string
     */
    public function getSql($withParams = false){
        $from = $this->deleteSpaces($this->queryBuilder['from']);
        $fromLetter = $this->queryBuilder['short'] ?? '';
        $dot = empty($fromLetter) ? '' : '.';
        if ($this->queryBuilder['enabled'] ?? false) $this->where($fromLetter.$dot.'enabled', 1);

        $leftJoins = [];
        foreach ($this->queryBuilder['leftJoin'] as $left) {
            if ($left[6] == 'left join') {
                $leftJoins[] = "LEFT JOIN $left[0] $left[1] ON $left[2] $left[3] $left[4]";
                if ($left[5]) $this->where($left[1] . '.enabled', 1);
            } else if ($left[6] == 'outer apply') {
                $enabled = $left[5] ? 'AND enabled = 1': '';
                $order = $left[7] ? " ORDER BY $left[7]": '';
                $leftJoins[] = "OUTER APPLY (SELECT TOP 1 * FROM $left[0] $left[1] WHERE $left[2] $left[3] $left[4] $enabled $order) $left[1]";
            }
        }

        $wheres = [];
        foreach ($this->queryBuilder['where'] as $where) {
            $value = '?';//$this->getFilterFunction($where[3]);
            $wheres[] = $where[0].' '.$where[1].' '.$value.' ';
        }

        $wheres = array_merge($wheres, $this->queryBuilder['whereRaw']);

        $fields = !empty($this->queryBuilder['fields']) ? $this->queryBuilder['fields'] : ['*'];
        $fields = $this->deleteSpaces($fields);

        $top1 = $this->queryBuilder['first'] ? 'top 1 ' : '';

        $sql = $this->isPagination ? '' : $this->getVariableStr();

        $sql .= "SELECT $top1".implode("$this->sep , ", $fields).$this->sep;
        if (!empty($from)) $sql .= "FROM $from $fromLetter$this->sep";
        if (!empty($leftJoins)) $sql .= implode($this->sep, $leftJoins);
        if (!empty($wheres)) $sql .= $this->sep."WHERE ".implode($this->sep.' AND ', $wheres);
        if (!empty($this->queryBuilder['groupBy'])) $sql .= $this->sep."GROUP BY ".implode(', ', $this->queryBuilder['groupBy']);
        if (!$this->isPagination && isset($this->queryBuilder['orderBy'])){
            $o = $this->queryBuilder['orderBy'];
            $sql .= $this->sep."ORDER BY $o[0] $o[1]";
        }

        if ($withParams){
            foreach ($this->getWhereArgs() as $where){
                $sql = preg_replace('/\?/', "'$where'", $sql, 1);
            }
        }

        return $sql;
    }

    function getVariableStr(){
        $vars = [];
        foreach ($this->queryBuilder['vars'] as $var) {
            $vars[] = "DECLARE $var[1] $var[0]; SET $var[1] = $var[2]; ";
        }
        if (!empty($vars)){
            return implode(' ', $vars);
        } else {
            return '';
        }
    }

    /**
     * Удаляет пробелы для безопасности
     * Заменяет __ на пробел, это если нужно обойти безопасность)
     * @param $arrayOrStr
     * @return array|string|string[]|null
     */
    private function deleteSpaces($arrayOrStr){
        if($this->fromRequest){
            if (is_array($arrayOrStr)){
                return array_map(function ($item){
                    $item = preg_replace("/ +/", '', $item);
                    return preg_replace("/__/", ' ', $item);
                }, $arrayOrStr);
            } else {
                $arrayOrStr =  preg_replace("/ +/", '', $arrayOrStr);
                return preg_replace("/__/", ' ', $arrayOrStr);
            }
        } else {
            return $arrayOrStr;
        }
    }

    private function getWhereArgs(){
        $whereArgs = [];
        foreach ($this->queryBuilder['where'] as $where) {
            $whereArgs[] = $where[2];
        }
        return $whereArgs;
    }

    private function resetQueryBuilder(){
        $this->queryBuilder = [
            'fields' => [],
            'from' => '',
            'where' => [],
            'leftJoin' => [],
            'first' => false,
            'orderBy' => null,
            'groupBy' => [],
            'whereRaw' => [],
            'vars' => []
        ];
    }

//    private function getFields($isOnlyDef = false){
//        $reqFields = $_REQUEST['fields'] ?? [];
//        if (!is_array($reqFields)) response(['error'=>400, 'message'=>'Invalid array fields'])->send();
//        $def = $this->default;
//        function getFields($defFields, $reqFields, $skip = false){
//            return array_map(function ($field) use ($reqFields, $skip){
//                $name = explode('.', $field['name']);
//                $name = count($name) == 2? $name[1] : $name[0];
//                if ($skip || empty($reqFields) || array_search($name, $reqFields) !== false){
//                    $db = $field['dbName'] ?? $field['name'];
//                    $db = isset($field['date']) && $field['date'] ? "case__when__{$db}__>__0__then__dbo.DateToShortDateStr($db)__else__''__end":$db;
//                    $db = isset($field['datetime']) && $field['datetime'] ? "case__when__{$db}__>__0__then__dbo.DateToDateStr($db)__else__''__end":$db;
//                    $db = isset($field['time']) && $field['time'] ? "dbo.FormatDateTime('HH:mm:ss', $db)":$db;
//                    return $db.'__as__'.$name;
//                }
//            }, $defFields);
//        }
//        if (isset($def['fields'])){
//            $fields = getFields($def['fields'], $reqFields, $isOnlyDef);
//            $fields = array_filter($fields, function ($item) { return $item != null; });
//            if (empty($fields)){
//                $fields = getFields($def['fields'], $reqFields, true);
//            }
//        } else {
//            $fields = ['*'];
//        }
//
//        return $fields;
//    }

//    private function getFieldByName($regField){
//        if (isset($this->default['fields'])){
//            foreach ($this->default['fields'] as $field) {
////                $name = explode('.', $field['name']);
////                $name = count($name) == 2? $name[1] : $name[0];
//                if ($regField == $this->getDotName($field['name'])){
//                    return $field;
//                }
//            }
//        }
//        return false;
//    }

//    private function getFilterFunction($field){
//        if ((isset($field['date']) && $field['date']) || (isset($field['datetime']) && $field['datetime'])){
//            return 'dbo.datestrtodate(?)';
//        }
//        if ((isset($field['time']) && $field['time'])){
//            return '\'0\'+ SUBSTRING(STR(dbo.datestrtodate(?), 17, 17), 6, 100)';//todo: для времени надо что-то придумать лучше
//        }
//        return '?';
//    }
//    public function updateFromRequest($request, $table, $id){
//        $q = [];
//        $v = [];
//        foreach ($request->all() as $key => $value) {
//            $field = $this->getFieldByName($key);
//            if (!$field || !isset($field['update']) || !$field['update']) return false;
////            $name = $field['dbName'] ?? $field['name'];
////            $name = explode('.', $name);
////            $name = count($name) == 2? $name[1] : $name[0];
//            $name = $this->getDotName($field['dbName'] ?? $field['name']);
//            $q[] = "$name = {$this->getFilterFunction($field)}";
//            $v[] = $value;
//        }
//        if(empty($q)) return false;
//        $sql = "update $table set ".implode(', ', $q). " where id = $id";
//        return $this->execute($sql, $v);
//    }

    public function execute($sql, $args, $fetch = false){
        $stmt = $this->db->getPdo()->prepare($sql);
        foreach ($args as $key => $arg) {
            $bindKey = is_numeric($key) ? $key + 1 : $key;
            $stmt->bindValue($bindKey, $arg);
        }
        try {
            $res = $stmt->execute();
        } catch (\Exception $exception){
            if ($this->errorHandler) ($this->errorHandler)('db','Query error. '. $exception->getMessage());
            if (env('APP_DEBUG', false)){
                return response()->json($exception->getMessage(), 400)->send();
            }
            return response($this->errorSqlResponse, 400)->send();
        }

        if ($fetch) $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $res;
    }

//    private function getDotName($name){
//        $name = explode('.', $name);
//        return count($name) == 2? $name[1] : $name[0];
//    }

    /**
     * Проверяет существует ли таблица или поле
     * @param string $table
     * @param string|null $column
     * @return bool
     */
    public function exists($table, $column = null){
        if ($column){
            $res =  $this->db->select("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = '$table' AND COLUMN_NAME = '$column'");
        } else {
            $res =  $this->db->select("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
            WHERE TABLE_NAME = '$table' ");
        }
        return $res && count($res) > 0;
    }

    public function getSysValue($name){
        return $this->execute("select dbo.get_sys_value('$name') as value",[], true)[0]['value'];
    }

    private function wrapSqlFunction($value){
        if (preg_match_all("/(dbo\..*)\((.*)\)/", $value, $match)){
            $params = $match[2][0] ? explode(',', $match[2][0]) : [];
            $val = implode(',', array_fill(0, count($params), '?'));
            return ['s' => "{$match[1][0]}($val)", 'params' => $params];
        }
        return ['s' => "?", 'params' => [$value]];
    }

    public function update($tableName, $id, $values, $idKey = 'id'){
        if (empty($values)) return false;
        $params = array();
        foreach ($values as $key => $value){
            $wrap = $this->wrapSqlFunction($value);
            $keys[] = "$key = ".$wrap['s'];
            $params = array_merge($params, $wrap['params']);
        }
        $params[] = $id;
        return $this->execute("update $tableName set ". implode(', ', $keys). " where $idKey = ?", $params);
    }

}
