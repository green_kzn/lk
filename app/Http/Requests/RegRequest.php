<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:8|confirmed',
            // 'password_confirmation' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'Пароль является обязательным',
            // 'password_confirmation.required'  => 'Подтверждение пароля является обязательным',
            'password.min'  => 'Пароль должен содержать не менее 8 символов',
            // 'password_confirmation.confirmed'  => 'Пароли не совпадают',
        ];
    }
}
