<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomLoginController extends Controller
{

    public function index(Request $request) {
        return view('auth.login', self::getVars());
    }

    public function login(Request $request)
    {
        $phone = $request->phone;
        if (in_array(substr($phone, 0, 1) , ['8','7'])){
            $phone = '+7'.substr($phone,1);
        }
        $arr = array("-", "(", ")", " ", "+7");
        $phone = str_replace($arr, "", $phone);
        $credentials = array(
            'phone' => $phone,
            'password' => $request->password,
            'domain' => $request->getHttpHost()
        );

        $credentialsWithoutDomain = array(
            'phone' => $phone,
            'password' => $request->password,
            'domain' => null
        );

        if (Auth::attempt($credentials) || Auth::attempt($credentialsWithoutDomain)) {
            if (!Auth::user()->domain) Auth::user()->update(['domain' => $request->getHttpHost()]);
            // Authentication passed...
            return redirect()->intended('/home');
        } else {
            return redirect()->route('login')->withErrors('Пользователь с таким логином и паролем не найден');
        }
    }

    static function getVars(){
        $colors = Color::get();
        $img = file_get_contents(public_path('img/right.svg'));
        $img = str_replace('#FF0000', $colors['main'], $img);
        $rightImg = 'data:image/svg+xml;base64, '.base64_encode($img);
        $colors['rightImg'] = $rightImg;
        return $colors;
    }
}
