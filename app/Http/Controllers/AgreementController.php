<?php

namespace App\Http\Controllers;

class AgreementController extends Controller {

    public function index() {
        $settings = InfoHelper::getClinicInLocalDb()->settings;

        if (isset($settings->agreement)) {
            return file_get_contents($settings->agreement);
        } else {
            $clinic = InfoHelper::getClinicInfo();
            return view('agreement.common', compact('clinic'));
        }
    }

}
