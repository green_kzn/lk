<?php

namespace App\Http\Controllers;
use App\Http\Helpers\AdminApi;
use DB;
use Crypt;
use Illuminate\Http\Request;

class ConnController extends Controller
{
    public function index(Request $request){
//        $clinic = InfoHelper::getClinicInLocalDb();
        if ($request->getClientIp() != '46.254.17.80'){
            abort(404);
        }
        $clinicId = $request->clinic_id;
        if (isset($clinicId)){
            if (is_numeric($clinicId)){
                $data = DB::connection('mysql_adm')
                    ->select("select conn_str from connections where id = $clinicId");

                if (!empty($data)){
                    $decrypted = Crypt::decrypt($data[0]->conn_str);

                    $conn = explode('||', $decrypted);
                    $conn = [
                        'host' => $conn[0],
                        'port' => $conn[1],
                        'database' => $conn[2],
                        'username' => $conn[3],
                        'password' => $conn[4],
                    ];

//                ReversCrypt::mc_encrypt($conn);

                    return (base64_encode(json_encode($conn)));
                } else {
                    abort(404);
                }
            } else {
                $client = AdminApi::get(base64_encode($clinicId));

                if ($client) {

                    $conn = $this->changeLocalIp($client->connection);
                    $conn = [
                        'host' => $conn->host,
                        'port' => $conn->port,
                        'database' => $conn->db,
                        'username' => $conn->login,
                        'password' => $conn->pass,
                    ];

//                ReversCrypt::mc_encrypt($conn);

                    return (base64_encode(json_encode($conn)));
                } else {
                    abort(404);
                }
            }
        } else {
            abort(404);
        }

    }

    /*
     * Меняем локальный IP 192.168.1.105 на внешний 94.180.249.180, так надо не спрашивай почему
     */
    function changeLocalIp($conn){
        if ($conn->host == '192.168.1.105'){
            $conn->host = '94.180.249.180';
            $conn->port = '11433';
        }
        return $conn;
    }
}
