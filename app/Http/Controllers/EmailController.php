<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class EmailController extends Controller
{
    static function sendMail($mailTo,$from,$fromName,$message,$subject)
    {
        $to  = $mailTo;
        $subject = '=?utf-8?b?'. base64_encode($subject) .'?=';
        $fromMail = $from;
        $date = date(DATE_RFC2822);
        $messageId='<'.time().'-'.md5($from.$mailTo).'@'.$_SERVER['SERVER_NAME'].'>';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= "Content-type: text/html; charset=utf-8". "\r\n";
        $headers .= "From: ". $from ." <". $from ."> \r\n";
        $headers .= "Date: ". $date ." \r\n";
        $headers .= "Message-ID: ". $messageId ." \r\n";

        return mail($mailTo, $subject, $message, $headers);
    }

    static function sendMailAttachment($mailTo, $from, $subject, $message, $contentFile = false, $fileName = false, $filepath = false){
//        $separator = "-----"; // разделитель в письме
        $separator = "--".md5(uniqid(time()));
        $date = date(DATE_RFC2822);
        $subject = '=?utf-8?b?'. base64_encode($subject) .'?=';
        $messageId='<'.time().'-'.md5($from.$mailTo).'@'.$_SERVER['SERVER_NAME'].'>';
        // Заголовки для письма
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "From: ". $from ." <". $from ."> \r\n";// "From: $from\nReply-To: $from\n"; // задаем от кого письмо
        $headers .= "Date: ". $date ." \r\n";
        $headers .= "Content-Type: multipart/mixed; boundary=\"$separator\"; charset=utf-8". "\r\n"; // в заголовке указываем разделитель
        $headers .= "Message-ID: ". $messageId ." \r\n";
        // если письмо с вложением
        if($filepath || $contentFile){
            $bodyMail = "--$separator\r\n"; // начало тела письма, выводим разделитель
            $bodyMail .= "Content-Type:text/html; charset=utf-8\r\n"; // кодировка письма
            $bodyMail .= "Content-Transfer-Encoding: 7bit\n\n"; // задаем конвертацию письма
            $bodyMail .= $message."\r\n"; // добавляем текст письма
            $bodyMail .= "--$separator\r\n";
            if (!$contentFile){
                $fileRead = fopen($filepath, "r"); // открываем файл
                $contentFile = fread($fileRead, filesize($filepath)); // считываем его до конца
                fclose($fileRead); // закрываем файл
                $fileName = basename($filepath);
            }
            $bodyMail .= "Content-Type: application/octet-stream; name==?utf-8?B?".base64_encode($fileName)."?=\r\n";
            $bodyMail .= "Content-Transfer-Encoding: base64\r\n"; // кодировка файла
            $bodyMail .= "Content-Disposition: attachment; filename==?utf-8?B?".base64_encode($fileName)."?=\n\n";
            $bodyMail .= chunk_split(base64_encode($contentFile))."\r\n"; // кодируем и прикрепляем файл
            $bodyMail .= "\r\n--$separator--\r\n";
            // письмо без вложения
        }else{
            $headers .= "Content-type: text/html; charset=utf-8". "\r\n";
            $bodyMail = $message;
        }

        $result = mail($mailTo, $subject, $bodyMail, $headers); // отправка письма
        return $result;
    }

    static function KMail($to, $from, $subject, $message, $filename = null, $filepath = null, $isHTML = false){
        $boundary = "--".md5(uniqid(time()));
        // генерируем разделитель

        $mailheaders = "MIME-Version: 1.0;\r\n";
        $mailheaders .="Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n";
        // разделитель указывается в заголовке в параметре boundary

        $mailheaders .= "From: $from <$from>\r\n";
        $mailheaders .= "Reply-To: $from\r\n";

        $multipart = "--$boundary\r\n";
        $multipart .= "Content-Type: text/html; charset=windows-1251\r\n";
        $multipart .= "Content-Transfer-Encoding: base64\r\n";
        $multipart .= "\r\n";
        $multipart .= chunk_split(base64_encode(iconv("utf8", "windows-1251", $message)));
        // первая часть само сообщение

        // Закачиваем файл
        $fp = fopen($filepath,"r");
        if (!$fp)
        {
            print "Не удается открыть файл";
            exit();
        }
        $file = fread($fp, filesize($filepath));
        fclose($fp);
        // чтение файла


        $message_part = "\r\n--$boundary\r\n";
        $message_part .= "Content-Type: application/octet-stream; name=\"$filename\"\r\n";
        $message_part .= "Content-Transfer-Encoding: base64\r\n";
        $message_part .= "Content-Disposition: attachment; filename=\"$filename\"\r\n";
        $message_part .= "\r\n";
        $message_part .= chunk_split(base64_encode($file));
        $message_part .= "\r\n--$boundary--\r\n";
        // второй частью прикрепляем файл, можно прикрепить два и более файла

        $multipart .= $message_part;

        if (mail($to,$subject,$multipart,$mailheaders)) {
            return true;
        } else {
            return false;
        };
        // отправляем письмо

        // удаляем файлы через 60 сек.
        // if (time_nanosleep(5, 0)) {
        //         unlink($filepath);
        // }
        // удаление файла
    }
}
