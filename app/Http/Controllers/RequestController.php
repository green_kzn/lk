<?php

namespace App\Http\Controllers;

use App\Http\Controllers\SendGrid\Email;
use App\Http\Controllers\SendGrid\SendGrid;
use App\Http\Helpers\ClientDB;
use App\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Session;

class RequestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function send(Request $request) {
        $phone = $request->phone;
        $name = MainController::getCurrentUser()->FULLNAME ?? $request->name;
        $clinicEmail = InfoHelper::getClinicEmail();
        $this->createLead($name, $phone);

        if (!empty($clinicEmail)) {
            $from = 'noreply@archimed-soft.ru';
            $message = "<h2>Заявка с личного кабинета</h2>
            <p>Пользователь оставил заявку на портале личного кабинета.</p>
            <p><b>Имя:</b> ".$name."</p>
            <p><b>Телефон:</b> {$phone}</p>";

            $result = EmailController::sendMail($clinicEmail, $from, $from , $message, 'Заявка с личного кабинета');

            return array('success' => $result);
        }
        return response()->json(array('success' => false, 'msg' => 'Ошибка отправки заявки'));
    }

    function createLead($name, $phone){
        $db = new ClientDB();
        $phone = InfoHelper::handlePhone($phone);

        $db->execute("
            DECLARE @newId int;
            DECLARE @userId int;
            DECLARE @now float;
            set @userId = dbo.get_sys_value_int('IR.USERREGISTR');
            set @now = dbo.current_now();

            EXEC crm_set_leads_new_call
              ?,    -- @FullName varchar(250)
              ?,   -- @Phone varchar(20)
              '',         -- @Email varchar(20)
              'Заявка с личного кабинета', -- @Info varchar(500)
              @userId,    -- @User int
              0,          -- @ISource int
              0,          -- @Source int
              0,          -- @State int
              0,          -- @McId int
              0,          -- @Id int
              @now,       -- @Date float
              0,          -- @Kind int
              -1,         -- @CallReason int
              0,          -- @Responsible int
              @userId,    -- @OwnerId int,
              @newId out

            select @newId as id;
        ", [$name, $phone]);

        if ($db->exists('LEADS')){
            $leadId = $db->fields(['id'])
                ->from('LEADS')
                ->where('fullname', $name)
                ->where('phone', $phone)
                ->orderBy('id', 'desc')
                ->first();

            $leadId = $leadId['id'];

            $roistat = $_COOKIE['roistat_visit'] ?? null;
            if (isset($roistat) && $roistat){
                $db->update('LEADS', $leadId, ['LIDVISIT' => $roistat]);
            }
        }
    }
}
