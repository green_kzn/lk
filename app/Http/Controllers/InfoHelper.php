<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class InfoHelper extends Controller
{
    static function getClinicEmail(){
        return  Session::get('client')->settings->email;
    }

    static function getClinicSenderEmail(){
        return 'noreply@archimed-soft.ru';
    }

    static function getClinicInfo($img = false, $fullAddress = true){
        $settings = Session::get('client')->settings;
        $str = '';
        if (isset($settings->hospital_id) && $settings->hospital_id > 0) {
            $str = "and h.id = $settings->hospital_id";
        }
        $imgStr = $img ? ', h.imglogo ' : '';

        $clinic = self::getClinic($imgStr, $str);
        if (!$clinic) $clinic = self::getClinic($imgStr, '');
        $clinic->address = self::getAddress($clinic, $fullAddress);
        $clinic->logoLink = $settings->logo_link ?? '';
        self::setDomains($clinic, $settings);
        return $clinic;
    }

    private static function setDomains(&$clinic, $settings){
        $clinic->irDomains = json_decode($settings->ir_domains ?? '[]') ?? [];
        if(empty($clinic->irDomains) && isset($settings->ir_domain)){
            $clinic->irDomains = [(object)['domain' => $settings->ir_domain,'name'=>'']] ?? [];
        }
        foreach ($clinic->irDomains as &$item) {
            $item->domain = self::handleUrl($item->domain);
        }
    }

    private static function getClinic($imgStr, $str){
        $clinic = DB::connection('sqlsrv')
            ->select("
            SELECT top 1    h.name,
            --dbo.get_full_org_address(o.id) as address,
            h.address_zone, h.address_locality, h.address,
            h.address_house, h.address_build, h.address_build1, h.address_flat,
            h.phone,
            h.www,
            h.email,
            oml.name as license,
            dbo.FormatDateTime('dd.MM.yyyy',oml.date_issue) as licenseDate
            $imgStr
            FROM hospitals h
            LEFT JOIN organizations o ON o.id = h.orgid
            LEFT JOIN ORGANIZATION_MEDICAL_LICENSES oml ON ((oml.ORGANIZATIONID = o.id)
                                                        and (oml.enabled = 1))
            LEFT JOIN organizations b ON o.BANKID = b.id
            LEFT JOIN countryes c ON c.id = o.country
            WHERE h.enabled = 1 $str");

        if (empty($clinic)){
            return false;
        }
        return $clinic[0];

    }

    static function getClinicInfoForPdfHeader(){
        $info = self::getClinicInfo(true);

        $result = '';
        $usualArray = ['name', 'address', 'phone', 'www', 'email'];

        foreach ($info as $key => $item){
            if (in_array($key, $usualArray)){
                $item = trim($item);
                if (!empty($item)){
                    $result .= $item . "\n";
                }
            }
        }

        if (!empty($info->license)){
            $result .= "Лицензия {$info->license} от {$info->licenseDate}  г.\n";
        }
        $imgFile = public_path('img/logo.png');
        if (!empty($info->imglogo)){
            $imgFileName = Str::slug($info->name);
            $imgFile = storage_path()."/user_files/$imgFileName.jpg";
            $handle = fopen($imgFile, 'w');
            fwrite($handle, $info->imglogo);
            fclose($handle);
        }

        return array(
            'info' => mb_substr($result, 0, -1),
            'logo' => $imgFile
        );
    }


    static function getClinicInLocalDb(){
        return Session::get('client');
    }

    static function getSettings($name = null){
        $client = self::getClinicInLocalDb();
        if ($name){
            return $client->settings->{$name} ?? null;
        }
        return  $client->settings;
    }

    static function getGenderStr($genderType){
        switch ($genderType) {
            case 0:
                $sex = 'Мужской';
                break;
            case 1:
                $sex = 'Женский';
                break;
            default:
                $sex = ' ';
                break;
        }
        return $sex;
    }

    static function getAddress($obj, $full = true) {
        $res = [];
        if ($full){
            if (!empty($obj->address_state))
                $res[] = $obj->address_state . ' обл.';
            if (!empty($obj->address_zone))
                $res[] = $obj->address_zone . ' р-он.';
        }
        if (!empty($obj->address_locality))
            if (preg_match("/г\./", $obj->address_locality)) {
                $obj->address_locality_type_name = '';
            }
            $res[] = ($obj->address_locality_type_name ?? 'г. ') . $obj->address_locality;
        if (!empty($obj->address))
            $res[] = ($obj->address_type_name ?? 'ул. ') . $obj->address;
        if (!empty($obj->address_house))
            $res[] = 'д. ' . $obj->address_house;
        if (!empty($obj->address_build))
            $res[] = 'корп. ' . $obj->address_build;
        if (!empty($obj->address_build1))
            $res[] = 'стр. ' . $obj->address_build1;
        if (!empty($obj->address_flat))
            $res[] = 'оф. ' . $obj->address_flat;
        return implode(', ', $res);
    }

    static function handlePhone($phone){
        $phone = preg_replace("/[^\d]/", '', $phone);
        $length = strlen($phone);
        if ($length == 10){
            return '+7'.$phone;
        } elseif ($length == 11){
            $first = substr($phone, 0, 1);
            if ($first == '8'){
                return '+7'.substr($phone, 1);
            } elseif ($first == '7'){
                return '+'.$phone;
            } else
                return $phone;
        } elseif (!preg_match("/\+/", $phone)){
            return '+'.$phone;
        } else {
            return $phone;
        }
    }

    static function handleUrl($url){
        if (!preg_match("/http/", $url) && !empty($url)){
            $url = 'http://'.$url;
        }
        return $url;
    }

}
