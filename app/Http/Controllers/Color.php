<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class Color {

    static function get(){
        $client = InfoHelper::getClinicInLocalDb();

        return [
            'main' => $client->settings->main_color ?? 'red',
            'shadow' => $client->settings->shadow_color ?? 'rgba(255, 0, 0, 0.05)',
        ];
    }

}
