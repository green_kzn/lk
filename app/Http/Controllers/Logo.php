<?php


namespace App\Http\Controllers;


use App\Http\Middleware\Visitors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class Logo extends Controller {

    private $ownLogo;

    public function index(){
        $this->ownLogo = InfoHelper::getClinicInLocalDb()->settings->logo ?? false;
        if ($this->ownLogo){
            return file_get_contents($this->getLogoUrl());
        } else {
            return file_get_contents(public_path('img/logo.png'));
        }
    }

    public function getLogoUrl(){
        $logo = Cookie::get('logo');
        if (!$logo){
            (new Visitors())->handle(Request::capture(), function (){});
            $logo =  InfoHelper::getClinicInfoForPdfHeader()['logo'];
            Cookie::queue('logo', $logo, 1440);
        }
        return $logo;
    }

}
