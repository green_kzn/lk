<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;

class FinancialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        return view('home');
    }

    public function getFin() {
        $services = DB::connection('sqlsrv')
                ->select("
                SELECT serv.name
                    , ac.costall
                    , dbo.DateToShortDateStr(ac.CREATIONDATE) as date
                    , ac.quant
                    , ac.checkState
                    , ac.paymentCost
                    , ac.restCost
                FROM ACCOUNTS_CONTENTS ac
                LEFT JOIN ACCOUNTS a on ac.ACCOUNT = a.ID
                LEFT JOIN SERVICES serv on ac.SERVICE = serv.ID
                WHERE a.mcid = ".MainController::getCurrentUser()->ID."
                AND ac.ENABLED = 1
                AND ac.quant = 1
                ORDER BY ac.CREATIONDATE DESC");

        foreach ($services as $service){
            $service->quant = round($service->quant);
            $service->costall = round($service->costall, 2);
            $service->paymentCost = round($service->paymentCost, 2);
            $service->restCost = round($service->restCost, 2);
        }
        return response()->json(array(
            'services' => $services
        ));
    }
}
