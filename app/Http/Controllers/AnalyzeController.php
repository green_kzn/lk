<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use Session;
use DOMPDF as PDF;

class AnalyzeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getAnalyze() {
        $analyze = DB::connection('sqlsrv')
                ->select("SELECT dbo.DateToShortDateStr(lres.CREATIONDATETIME) as date,
                dbo.DateToShortTimeStr(lres.CREATIONDATETIME) as time,
                lres.code, lrl.[NAME] as Name
                FROM LABORATORY_RESEARCHES lres
                LEFT JOIN medCards mc ON mc.id = lres.McId
                left join LR_G lrg on lrg.id = lres.LRGID
                left join lr_list lrl on lrl.id = lres.LRLID
                left join LR_RESULT lrr on lrr.LRID = lres.ID
                WHERE lres.Enabled = 1 AND
                mc.id = ".MainController::getCurrentUser()->ID." AND
                lres.NotExec = 0 and
                lrr.ID IS NOT NULL
                group by lres.CREATIONDATETIME, lres.McId, lres.code, lrl.[NAME]
                order by lres.CREATIONDATETIME desc");

        return response()->json($analyze);
    }

    public function pdf($code){
        $data = $this->getAnalyzePdfData($code);
        if ($data){
            if ($data['analyze'][0]->has_print_form){
                $id = $data['analyze'][0]->epmz_id;
                $dataContent = $this->getPrintFormPdf($id);
                $filename = "epmz_{$data['user']->ID}-{$id}.pdf";
                Storage::disk('local')->put($filename, $dataContent);
                header("Content-type: application/pdf");
                header("Content-disposition: filename=epmz_{$data['user']->ID}-{$id}.pdf");
                echo (file_get_contents(storage_path('app/public/'.$filename)));
            } else {
                $pdf = PDF::loadView('pdf.analyze', $data);
                return $pdf->stream();
            }
        } else {
            abort(404);
        }
    }

    public function send($code){
        $user = MainController::getCurrentUser();
        if (isset($user->EMAIL) && filter_var($user->EMAIL, FILTER_VALIDATE_EMAIL)) {
            $clinicEmail = InfoHelper::getClinicSenderEmail();

//            $filepath = storage_path()."/analyses/analyze_result_{$user->ID}-{$code}.pdf";
            $fileName = "analyze_result_{$user->ID}-{$code}.pdf";

            $data = $this->getAnalyzePdfData($code);
            if (!$data){
                return response()->json(array(
                    'status' => false,
                    'message' => 'Ошибка отравки e-mail'
                ));
            }

            if ($data['analyze'][0]->has_print_form){
                $id = $data['analyze'][0]->epmz_id;
                $fileContent = $this->getPrintFormPdf($id);
            } else {
                $pdf = PDF::loadView('pdf.analyze', $data);
                $fileContent = $pdf->output();
            }

            $result = EmailController::sendMailAttachment(
                $user->EMAIL,
                $clinicEmail,
                'Результаты лабораторного исследования',
                view('pdf.analyze', $data, ['noHeader' => true]),
                $fileContent,
                $fileName
            );

//            unlink($filepath);
            if($result) {
                return response()->json(array(
                    'status' => $result,
                    'message' => 'Результаты успешно отправлены на Ваш e-mail'
                ));
            }
        }
        return response()->json(array(
            'status' => false,
            'message' => 'Не указан корректный e-mail'
        ));

    }

    private function getAnalyzePdfData($code){
        $user = MainController::getCurrentUser();

        $exists = DB::connection('sqlsrv')
            ->select("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
            WHERE TABLE_NAME = 'epmz_types_default_forms' ");

        $exists = $exists && count($exists) > 0;

        $analyze = DB::connection('sqlsrv')
            ->select("SELECT dbo.DateToShortDateStr((floor(lres.ISDONEDATETIME))) AS DoneDate
                , lltf.Name                                  AS FieldName
                , lr.StrValue
                , lr.NormText
                , dbo.DateToShortDateStr(FLOOR(lrg.creationdatetime)) AS visit_date
                , ll.name
                , e.id as epmz_id
                , ".($exists ? 'isnull(etdf.id, 0)' : 0)." as has_print_form
             FROM           laboratory_researches        lres
                  LEFT JOIN laboratory_researches_test   labrt  ON lres.Id        = labrt.LrId
                  LEFT JOIN lr_list_test                 llt    ON llt.id         = labrt.lrtestid
                  LEFT JOIN lr_list                      ll     ON ll.id          = lres.lrlid
                  LEFT JOIN lr_list_test_APP             llta   ON llta.lrtestid  = llt.id
                  LEFT JOIN lr_list_test_field           lltf   ON lltf.lrtid     = llt.id
                  LEFT JOIN lr_result                    lr     ON ((lr.lrtestid = llt.id)AND (lr.Enabled = 1)
           AND (lr.LrTestFieldId = lltf.Id) AND (lr.LrId = labrt.LrId))
                  LEFT JOIN lr_probs                     lp     ON lp.Id          = lres.lrprobid
                  LEFT JOIN lr_prob_types                lpt    ON lpt.Id         = lp.probtype
                  LEFT JOIN lr_list_test_field_norm      lltfn  ON lltfn.Id       = lr.LrTestFieldNormId
                  LEFT JOIN users      u  ON lres.isdoneuserid       = u.id
                  LEFT JOIN lr_g lrg      ON lrg.id = lres.LRGID
                  LEFT JOIN medCards mc ON mc.id = lres.McId
                  LEFT JOIN epmz e ON e.id = lres.epmzid
                  ".($exists ? 'LEFT JOIN epmz_types_default_forms etdf ON etdf.epmztypeid = e.epmztype' : '')."
            WHERE labrt.Enabled = 1
            AND lres.Enabled = 1
            AND lres.code = '{$code}'
            AND mc.id = {$user->ID}
            AND lltf.Enabled = 1
            AND lres.NotExec = 0
            AND ((lr.StrValue is not null) and (lr.StrValue != ''))

            ORDER BY lres.id, llt.name,  lltf.ORDEROFFIELDS");

        if (empty($analyze)){
            return false;
        }

        $sex = InfoHelper::getGenderStr($user->GENDERTYPE);
        $clinicInfo = InfoHelper::getClinicInfoForPdfHeader();

        return [
            'user' => $user,
            'analyze' => $analyze,
            'sex' => $sex,
            'clinic' => $clinicInfo
        ];
//        return PDF::loadView('pdf.analyze', $data);
    }

    function getPrintFormPdf($epmzId){
        $clinic = InfoHelper::getClinicInLocalDb();
        $report = base64_encode(json_encode([
            'ClinicId' => $clinic->client->token,
            'EpmzId' => $epmzId
        ]));
        return file_get_contents("http://report.archimed-soft.ru/?report=$report&format=PDF");
    }
}
