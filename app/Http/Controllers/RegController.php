<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\QTSMSController as QT;
use App\Http\Requests\RegRequest;
use DB;
use Carbon\Carbon;
use App\Sms_code;
use App\User;
use Session;
use Hash;

class RegController extends Controller
{
    public function getCode(Request $request) {
        $data = $request->all();

        if (substr($data['phone'], 0, 1) === '8'){
            $data['phone'] = '+7'.substr($data['phone'],1);
        }

        $arr = array("-", "(", ")", " ", "+7");
        $phone = str_replace($arr, "", $data['phone']);

        $hospital = DB::connection('sqlsrv')
                ->select("select top 1 * from medcards where phone like '%".$phone."'");

        if (count($hospital)) {
            $send = Sms_code::where('phone', '=', '+7'.$phone)
                ->first();

            if (!empty(User::where('phone', $phone)->where('domain', $request->getHttpHost())->get()->toArray())) {
                return response()->json(['status' => false, 'sms_status' => 'Ваш номер телефона уже зарегистрирован']);
            }

            if ($send) {
                $now = Carbon::now();
                $diff = $send->created_at->addSeconds(30);
                // dd($now);
                if ($now > $diff) {
                    $r = rand(1000, 9999);
                    $sms_text = 'Code: '.$r;

                    // $sms_code = new Sms_code();
                    // $sms_code->phone = '+7'.$data->phone;
                    $send->code = $r;
                    $send->save();

                    $target = '+7'.$phone;
                    $sender = 'ArchiMed+';
                    $period = 600;

                    try {
                        $sms = new QT('37180','34050926','service.qtelecom.ru');
                        $result = $sms->post_message($sms_text, $target, $sender, 'x124127456', $period);

                        return response()->json(['status' => true, 'sms_status' => 'Смс с кодом подтверждения успешно отправлено']);
                    } catch(Exception $e) {
                        return response()->json(['status' => false, 'error_code' => 603, 'sms_status' => $e->getMessage()]);
                    }
                } else {
                    return response()->json(['status' => false, 'sms_status' => 'Смс с кодом подтверждения уже отправлено. Повторите попытку позже']);
                };
            } else {
                $r = rand(1000, 9999);
                $sms_text = 'Code: '.$r;

                $sms_code = new Sms_code();
                $sms_code->phone = '+7'.$phone;
                $sms_code->code = $r;
                $sms_code->save();

                $target = '+7'.$phone;
                $sender = 'ArchiMed+';
                $period = 600;

                try {
                    $sms = new QT('37180','34050926','service.qtelecom.ru');
                    $result = $sms->post_message($sms_text, $target, $sender, 'x124127456', $period);

                    return response()->json(['status' => true, 'sms_status' => 'Смс с кодом подтверждения успешно отправлено']);
                } catch(Exception $e) {
                    return response()->json(['status' => false, 'error_code' => 603, 'sms_status' => $e->getMessage()]);
                }
            };
        } else {
            $phone = DB::connection('sqlsrv')
                ->select("select TOP 1 PHONE from HOSPITALS");

            return response()->json(['status' => false, 'sms_status' => 'Номер телефона не найден. Обратитесь по номеру телефона '.$phone[0]->PHONE]);
        }
    }

    public function register(Request $request) {
        $data = $request->all();
        if ($data['phone'] == NULL) {
            return redirect()->back()->withInput()->withErrors(['code' => 'Не верно указан номер телефона']);
        };
        if ($data['code'] == NULL) {
            return redirect()->back()->withInput()->withErrors(['code' => 'Не верно указан код подтверждения']);
        };

        if (substr($data['phone'], 0, 1) === '8'){
            $data['phone'] = '+7'.substr($data['phone'],1);
        }
        $arr = array("-", "(", ")", " ");
        $phone = str_replace($arr, "", $data['phone']);

        $code = Sms_code::where('phone', '=', $phone)
            ->latest()
            ->first();
        // dd($phone);
        if (isset($code->code) && $code->code == $data['code']) {
            // return response()->json(['status' => true]);
            if (!empty(User::where('phone', substr($phone, 2))->where('domain', $request->getHttpHost())->get()->toArray())) {
                return redirect()->back()->withInput()->withErrors(['code' => 'Ваш номер телефона уже зарегистрирован']);
            } else {
                Session::put('phone', substr($phone, 2));
                $code->verify = 1;
                $code->save();
            }
            return redirect()->route('register');
        };

        // return response()->json(['status' => false]);
        return redirect()->back()->withInput()->withErrors(['code' => 'Код не найден']);
    }

    public function resetCode(Request $request) {
        $data = $request->all();

        if (substr($data['phone'], 0, 1) === '8'){
            $data['phone'] = '+7'.substr($data['phone'],1);
        }
        $arr = array("-", "(", ")", " ", "+7");
        $phone = str_replace($arr, "", $data['phone']);

        $hospital = DB::connection('sqlsrv')
                ->select("select top 1 * from medcards where phone like '%".$phone."'");

        $user = User::where('phone', $phone)->where('domain', $request->getHttpHost())->get();
        if (empty($user->toArray())){
            return response()->json(['status' => false, 'sms_status' => 'Номер телефона не зарегистрирован']);
        }

        if (count($hospital)) {
            $send = Sms_code::where('phone', '=', '+7'.$phone)
                ->first();

            $now = Carbon::now();
            $diff = $send ? $send->created_at->addSeconds(30) : '0';

            if ($now > $diff) {
                $r = rand(1000, 9999);
                $sms_text = 'Code: '.$r;

                // $sms_code = new Sms_code();
                // $sms_code->phone = '+7'.$data->phone;
                if (!$send){
                    $send = Sms_code::create(['phone' => '+7'.$phone, 'code' => $r]);
                } else {
                    $send->update(['code' => $r]);
                }

                $target = '+7'.$phone;
                $sender = 'ArchiMed+';
                $period = 600;

                try {
                    $sms = new QT('37180','34050926','service.qtelecom.ru');
                    $result = $sms->post_message($sms_text, $target, $sender, 'x124127456', $period);

                    return response()->json(['status' => true, 'sms_status' => 'Смс с кодом подтверждения успешно отправлено']);
                } catch(Exception $e) {
                    return response()->json(['status' => false, 'error_code' => 603, 'sms_status' => $e->getMessage()]);
                }
            } else {
                return response()->json(['status' => false, 'sms_status' => 'Смс с кодом подтверждения уже отправлено. Повторите попытку позже']);
            }
        } else {
            return response()->json(['status' => false, 'sms_status' => 'Номер телефона не найден']);
        }
    }

    public function resetPassword(Request $request) {
        $data = $request->all();

        if ($data['phone'] == NULL) {
            return redirect()->back()->withInput()->withErrors(['code' => 'Не верно указан номер телефона']);
        };
        if ($data['code'] == NULL) {
            return redirect()->back()->withInput()->withErrors(['code' => 'Не верно указан код подтверждения']);
        };

        if (substr($data['phone'], 0, 1) === '8'){
            $data['phone'] = '+7'.substr($data['phone'],1);
        }

        $arr = array("-", "(", ")", " ", "+7");
        $phone = str_replace($arr, "", $data['phone']);

        $code = Sms_code::query()->where('phone', '=', '+7'.$phone)
            ->where('created_at', '>=' , Carbon::now()->subDay())
            ->first();

        if (isset($code->code) && $code->code == $data['code']) {
            Session::put('phone', $phone);
            return redirect()->route('reset');
        }

        return redirect()->back()->withInput()->withErrors(['code' => 'Код не найден']);
    }

    public function reset() {
        if (Session::get('phone')){
            return view('auth.passwords.reset', CustomLoginController::getVars());
        }
        abort(404);
    }

    public function postReset(RegRequest $request) {
        $data = $request->all();
        $phone = Session::get('phone');

        if ($data['password'] == NULL) {
            return redirect()->back()->withInput()->withErrors(['code' => 'Не верно указан пароль']);
        };
        if ($data['password_confirmation'] == NULL) {
            return redirect()->back()->withInput()->withErrors(['code' => 'Не верно указано подтверждение пароля']);
        };

        $hospital = DB::connection('sqlsrv')
                ->select("select top 1 * from medcards where phone like '%".$phone."'");

        if (count($hospital)) {

        } else {
            return redirect()->back()->withErrors(['code' => 'Номер телефона не найден']);
        }

        $user = User::where('phone', '=', $phone)
            ->where('domain', $request->getHttpHost())
            ->first();

        $user->password = Hash::make($data['password']);
        if ($user->save()) {
            return redirect()->route('login')->withErrors(['code' => 'Ваш пароль успешно изменен']);
        } else {
            return redirect()->back()->withErrors(['code' => 'Произошла ошибка при изменеии пароля']);
        }
    }
}
