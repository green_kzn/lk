<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class ZapisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        return view('home');
    }

    public function getTalons()
    {
        $talons = DB::connection('sqlsrv')
            ->select("
                SELECT t.ID, t.NUMBER, dbo.DateToShortDateStr(t.SHIFTDATE) as SHIFTDATE, dt.NAME as doctype,
                d.FULLNAME as DOCTOR, dbo.DateToShortTimeStr(t.BEGINTIME) as BEGINTIME,
                dbo.DateToShortTimeStr(t.ENDTIME) as ENDTIME, r.[NAME] as room, b.[NAME] as corpus,
                b.[ADDRESS] as address, s.[NAME] as status, rt.[NAME] as type
                from TALONS t
                left join DOCTORS d on d.ID = t.DOCID
                left join ROOMS as r on r.ID = t.ROOMID
                left join BUILDINGS b on b.id = r.BUILD
                left join TALON_STATUS s on s.id = t.[STATUS]
                left join TYPESRECEPTION rt on rt.ID = t.TYPERECEPTIONID
                left join DOCTORTYPES dt on dt.ID = d.[TYPE]
                where t.mcid = " . MainController::getCurrentUser()->ID . "
                and t.ENABLED = 1
                and (d.ENABLED = 1 or d.id is null)
                and t.shiftdate <> 0
                ORDER BY t.SHIFTDATE desc, t.BEGINTIME desc");

        return response()->json(array('talons' => $talons));
    }

}
