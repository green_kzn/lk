<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class ProgramController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        return view('home');
    }

    public function getProgram() {
        $user = MainController::getCurrentUser();
        $prog = DB::connection('sqlsrv')
                ->select("SELECT sp.id
                , sp.name
                --, sp.ISOK
                , case sp.ISOK when 1 then 'Окончено' else 'Не окончено' end as isok
                , dbo.DateToShortDateStr(sp.CREATIONDATETIME) as date
                , sp.expPeriod
                , u.NAME as usName
                , SUM(spc.COST * spc.QUANT) as price
            FROM SERVICE_PROGRAMM sp
            LEFT JOIN USERS u on u.id = sp.USERID
            LEFT JOIN SERVICE_PROGRAMM_CONTENT spc on spc.SERVICEPROGRAMMID = sp.id
            WHERE mcid = {$user->ID} and sp.[ENABLED] = 1
            GROUP BY sp.ID, sp.NAME, sp.ISOK, sp.CREATIONDATETIME, sp.ExpPeriod, u.NAME
            order by sp.CREATIONDATETIME desc");

        return response()->json($prog);
    }

    public function getProgramServices($id) {

        if (is_numeric($id)) {
            $user = MainController::getCurrentUser();

            $program = DB::connection('sqlsrv')
                ->select("SELECT sp.id
                , sp.name
                , case sp.ISOK when 1 then 'Окончено' else 'Не окончено' end as isok
                , dbo.DateToShortDateStr(sp.CREATIONDATETIME) as date
                , sp.expPeriod
                , u.NAME as usName
                , SUM(spc.COST * spc.QUANT) as price
            FROM SERVICE_PROGRAMM sp
            LEFT JOIN USERS u on u.id = sp.USERID
            LEFT JOIN SERVICE_PROGRAMM_CONTENT spc on spc.SERVICEPROGRAMMID = sp.id
            WHERE sp.id = $id
            and sp.mcid = {$user->ID}
            GROUP BY sp.ID, sp.NAME, sp.ISOK, sp.CREATIONDATETIME, sp.ExpPeriod, u.NAME");

            if (!empty($program)){
                $program = $program[0];
            } else {
                return response()->json(array(
                    'status' => false,
                    'message' => 'There\'s no such program'
                ));
            }

            $services = DB::connection('sqlsrv')
                ->select("SELECT s.code
                , s.name
                , spc.quant
                , spc.cost
                , (spc.quant * spc.cost) as costall
                , ac.quant as doneQuant

            FROM SERVICE_PROGRAMM_CONTENT spc
            LEFT JOIN SERVICE_PROGRAMM sp on sp.ID = spc.SERVICEPROGRAMMID
            LEFT JOIN SERVICES s on spc.SERVICEID = s.ID
            LEFT JOIN ACCOUNTS_CONTENTS ac ON ac.SERVICEPROGRAMMID = sp.ID and s.id = ac.service
            WHERE sp.ID = $id
            and sp.mcid = {$user->ID}
            and spc.ENABLED = 1");

            foreach ($services as $service) {
                $service->doneQuant = round($service->doneQuant);
                $service->quant = round($service->quant);
                $service->cost = round($service->cost, 2);
                $service->costall = round($service->costall, 2);
            }

            return response()->json(array(
                'services' => $services,
                'program' => $program
            ));
        } else {
            return response()->json(array(
                'status' => false,
                'message' => 'Invalid id'
            ));
        }
    }
}
