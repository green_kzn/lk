<?php

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function info(){
        $user = MainController::getCurrentUser();

        return response()->json(array(
            'hospital' => InfoHelper::getClinicInfo(false, false),
            'user' => $user,
            'users' => $this->mapUsers(),
            'roistat_key' => Roistat::getKey()
        ));

    }

    public static function getUser() {
        $temp = DB::connection('sqlsrv')
            ->select("select distinct m.ID, m.FULLNAME, dbo.DateToShortDateStr(m.BIRTHDATE) as BIRTHDATE, m.NUMBER,
                m.PHONE, m.AGE_YEARS, cl.CARDNUMBER, bb.AMOUNTBONUS, FLOOR(d.[VALUE]) as disc,
                m.ADDRESS_LOCALITY, m.ADDRESS_HOUSE, m.ADDRESS_BUILD, m.ADDRESS_FLAT, m.ADDRESS,
                dbo.DateToShortDateStr(MAX(t.SHIFTDATE)) as SHIFTDATE, m.GENDERTYPE, m.EMAIL,
                case when exists(select 1 from LABORATORY_RESEARCHES where enabled = 1 and mcid = m.id) then 1 else 0 end as lrExist,
                case when exists(select 1 from EPMZ where enabled = 1 and mcid = m.id and EPMZTYPE <> 8) then 1 else 0 end as epmzExist
                from medcards m
                left join BONUS_CARD_PUT card on m.id = card.MCID
                left join BONUS_CARD_LIST cl on cl.ID = card.CARDID
                left join BONUS_DATA_BUFFER bb on bb.CARDID = cl.ID
                left join talons t on t.mcid = m.id and t.enabled = 1 and (t.shiftdate+t.begintime) < dbo.current_now()
                left join DISCOUNTS d on d.id = m.DISCOUNTID
                where m.phone like '%".Auth::user()->phone."'
                and m.enabled = 1
                group by m.ID, m.FULLNAME, m.BIRTHDATE, m.NUMBER,
                m.PHONE, m.AGE_YEARS, cl.CARDNUMBER, bb.AMOUNTBONUS, d.[VALUE],
                m.ADDRESS_LOCALITY, m.ADDRESS_HOUSE, m.ADDRESS_BUILD, m.ADDRESS_FLAT, m.ADDRESS, m.GENDERTYPE, m.EMAIL");


        $users = [];
        foreach ($temp as $u) {
            $u->disc = round($u->disc);
            $users[$u->ID] = $u;
        }

        Session::put('users', $users);

        if (!Cookie::get('curUserId') && !empty($temp)){
            Cookie::queue('curUserId', $temp[0]->ID, 10000);
        }

//        Session::forget('user');
//        Session::put('user', $user);
        Session::put('update_time', Carbon::now());

        return MainController::getCurrentUser();
    }

    static function getCurrentUser(){
        $users =  Session::get('users');
        $user = $users[Cookie::get('curUserId')] ?? null;
        if (!$user && $users){
            $user = array_values($users)[0] ?? null;
            Cookie::queue('curUserId', $user->ID ?? null, 10000);
        }
        return $user;
    }


    private function mapUsers() {
        $res = [];
        foreach (Session::get('users') ?? [] as $u) {
            $res[] = [
                'id' => $u->ID,
                'name' => $u->FULLNAME
            ];
        }
        return $res;
//        return array_map(function ($u) {
//            return [
//                'id' => $u->ID,
//                'name' => $u->FULLNAME
//            ];
//        }, (array)Session::get('users') ?? []);
    }

    public function changeUser($id){
        if ($id>0){
            $users =  Session::get('users');
            $user = $users[$id] ?? null;
            if ($user){
                Cookie::queue('curUserId', $id, 100000);
            }
            return response()->json(array(
                'user' =>  $user ?? self::getCurrentUser()
            ));
        }

    }
}
