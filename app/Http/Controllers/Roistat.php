<?php

namespace App\Http\Controllers;

use App\Http\Helpers\AdminApi;
use Illuminate\Support\Facades\Cookie;

class Roistat {

    static function getKey(){
        $minutes = env('ROISTAT_UPDATE_TIME', 1460);
        $key = Cookie::get('my_roistat_key');

        if (!isset($key)){
            $token = InfoHelper::getClinicInLocalDb()->client->token;
            $client = AdminApi::getOtherProject(base64_encode($token), 'roistat');
            $key = $client->settings->key ?? '';
            Cookie::queue('my_roistat_key', $key, $minutes);
        }
        return $key;
    }

}
