<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;

class ExtractsController extends Controller
{
    protected $epmz = null;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        return view('home');
    }

    public function getEpmz($id = null) {
        $epmz = DB::connection('sqlsrv')
                ->select("select
                dbo.DateToDateStr(e.CREATIONDATETIME) as date,
                e.id as id,
                et.[NAME] as name,
                u.[NAME] as doctor
                ".(isset($id) ? ' , e.EXDATAXML' :' ')."
                from EPMZ e
                left join EPMZ_TYPES et on e.EPMZTYPE = et.id
                left join USERS u on u.id = e.CREATOR
                where e.[ENABLED] = 1
                and e.EPMZTYPE <> 8
                ".(isset($id) ? ' and e.id = '.$id.' ':'')."
                and e.MCID = ".MainController::getCurrentUser()->ID.
                " order by e.CREATIONDATETIME desc");

        return $epmz ;//response()->json($epmz);
    }

    public function pdf($id){
        if ($this->doesBelongsToPatient($id)){
            $user = MainController::getCurrentUser();
            $data = $this->getEpmzPdf($id);
            header("Content-type: application/pdf");
            header("Content-disposition: filename=epmz_{$user->ID}-{$id}.pdf");
            echo $data;
        } else {
            abort(404);
        }
    }

    public function doesBelongsToPatient($id){
        $epmz = DB::connection('sqlsrv')
            ->select("select e.id, et.name, dbo.DateToShortDateStr(e.CREATIONDATETIME) as date
                from EPMZ e
                left join EPMZ_TYPES et on e.EPMZTYPE = et.id
                where e.[ENABLED] = 1
                and e.id = $id
                and e.MCID = ".MainController::getCurrentUser()->ID);
        $this->epmz = $epmz[0] ?? null;
        return !empty($epmz);
    }

    public function send($id){
        if (!$this->doesBelongsToPatient($id)){
            abort(404);
        }
        $user = MainController::getCurrentUser();
        if (isset($user->EMAIL) && filter_var($user->EMAIL, FILTER_VALIDATE_EMAIL)) {
            $clinicEmail = InfoHelper::getClinicSenderEmail();

//            $filepath = storage_path()."/analyses/epmz_{$user->ID}-{$id}.pdf";
            $fileName = "epmz_{$user->ID}-{$id}.pdf";
            $fileContent = $this->getEpmzPdf($id);

            $result = EmailController::sendMailAttachment(
                $user->EMAIL,
                $clinicEmail,
                $this->epmz->date.', '.$this->epmz->name,
                '',
                $fileContent,
                $fileName
            );

//            unlink($filepath);
            if($result) {
                return response()->json(array(
                    'status' => $result,
                    'message' => 'Результаты успешно отправлены на Ваш e-mail'
                ));
            }
        }
        return response()->json(array(
            'status' => false,
            'message' => 'Не указан корректный e-mail'
        ));

    }

    function getEpmzPdf($epmzId){
        $clinic = InfoHelper::getClinicInLocalDb();
        $report = base64_encode(json_encode([
            'ClinicId' => $clinic->client->token,
            'EpmzId' => $epmzId
        ]));
        return file_get_contents("http://report.archimed-soft.ru/?report=$report&format=PDF");
    }
}
