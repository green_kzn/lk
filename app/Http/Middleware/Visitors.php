<?php

namespace App\Http\Middleware;

use App\Http\Controllers\InfoHelper;
use App\Http\Helpers\AdminApi;
use Closure;
use DB;
use Crypt;
use Session;

class Visitors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $client = AdminApi::getBy($request->getHttpHost(), 'site');
        if ($client) {
            $conn = $client->connection;
            $config = app()->make('config');
            $config->set('database.connections.sqlsrv', [
                'driver' => 'sqlsrv',
                'host' => $conn->host,
                'port' => $conn->port,
                'database' => $conn->db,
                'username' => $conn->login,
                'password' => $conn->pass,
                'charset' => 'cp1251',
                'collation' => 'Cyrillic_General_CI_AS',
                'prefix' => '',
                'strict' => false,
                'engine' => null,
            ]);
            Session::put('client', $client);
            return $next($request);
        }
        abort(404);
    }
}
