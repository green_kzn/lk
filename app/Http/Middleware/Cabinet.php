<?php

namespace App\Http\Middleware;

use App\Http\Helpers\AdminApi;
use App\Http\Controllers\MainController;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;
use DB;
use Crypt;
use Session;

class Cabinet
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $client = AdminApi::getBy($request->getHttpHost(), 'site');
        if ($client) {
            $conn = $client->connection;
            $config = app()->make('config');
            $config->set('database.connections.sqlsrv', [
                'driver' => 'sqlsrv',
                'host' => $conn->host,
                'port' => $conn->port,
                'database' => $conn->db,
                'username' => $conn->login,
                'password' => $conn->pass,
                'charset' => 'cp1251',
                'collation' => 'Cyrillic_General_CI_AS',
                'prefix' => '',
                'strict' => false,
                'engine' => null,
            ]);

            $this->checkUser($request);
            Session::put('client', $client);
            return $next($request);
        }
        return abort(404);
    }

    private function checkUser($request){
        $isInfo = $request->path() === 'info';

        $user = MainController::getCurrentUser();
        $lastUpdate = Session::get('update_time');
        $now = Carbon::now()->subMinutes(10);
        if ($isInfo || !isset($user) || !isset($lastUpdate) || $now > $lastUpdate ){
            MainController::getUser();
        }
    }
}
