<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms_code extends Model
{
    protected $fillable = ['phone', 'code'];
}
