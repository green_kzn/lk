function getDate(date, time){
    date = date.split('.')
    time = time.split(':')
    if (date && time){
        return new Date(parseInt(date[2]), parseInt(date[1])-1, parseInt(date[0]), parseInt(time[0]), parseInt(time[1]))
    }
}
function formatAddress(item){
    var address = ""
    if (typeof item !== 'undefined') {
        address += item['ADDRESS_LOCALITY'] ? item['ADDRESS_LOCALITY'] + ", " : ''
        address += item['ADDRESS'] ? item['ADDRESS'] + ' ' : ''
        address += item['ADDRESS_HOUSE'] ? item['ADDRESS_HOUSE'] : ''
        address += item['ADDRESS_BUILD'] ? item['ADDRESS_BUILD'] : ''
        address += item['ADDRESS_FLAT'] ? ', ' + item['ADDRESS_FLAT'] : ''
    }
    return address
}
